﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplates : MonoBehaviour
{
    public GameObject[] xRooms;
    public GameObject[] minusxRooms;
    public GameObject[] zRooms;
    public GameObject[] minuszRooms;

    public GameObject[] deadEndRooms;

    public GameObject closedRoom;

    public List<GameObject> rooms;
    public int maxRoom;
    public int minRoom;

    public float waitTime;
    private bool spawnedEndRoom;
    public GameObject End;

    private void Update()
    {
        if(waitTime <= 0 && spawnedEndRoom == false)
        {
            for(int i = 0; i < rooms.Count; i++)
            {
                if(i == rooms.Count - 1)
                {
                    int directionFrom = rooms[i].GetComponent<AddRoom>().directionFrom;
                    if (directionFrom == 1)
                    {
                        Instantiate(deadEndRooms[1], rooms[i].transform.position, Quaternion.identity);
                        Destroy(rooms[i].gameObject);
                    }
                    else if (directionFrom == 2)
                    {
                        Instantiate(deadEndRooms[0], rooms[i].transform.position, Quaternion.identity);
                        Destroy(rooms[i].gameObject);
                    }
                    else if (directionFrom == 3)
                    {
                        Instantiate(deadEndRooms[3], rooms[i].transform.position, Quaternion.identity);
                        Destroy(rooms[i].gameObject);
                    }
                    else if (directionFrom == 4)
                    {
                        Instantiate(deadEndRooms[2], rooms[i].transform.position, Quaternion.identity);
                        Destroy(rooms[i].gameObject);
                    }
                    Instantiate(End, rooms[rooms.Count-1].transform.position + new Vector3(0,0.5f,0), Quaternion.identity);
                    spawnedEndRoom = true;
                }
            }
        }
        else
        {
            waitTime -= Time.deltaTime;
        }
    }
}
