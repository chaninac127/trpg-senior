﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRoom : MonoBehaviour
{
    public int directionFrom; //direction given by the last room;
    // 1 -- > from x door;
    // 2 -- > from -x door;
    // 3 -- > from z door;
    // 4 -- > from -z door;


    private RoomTemplates templates;

    private void Start()
    {
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        templates.rooms.Add(this.gameObject);
    }
}
