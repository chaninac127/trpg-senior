﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
    public int openingDirection;
    // 1 -- > need x door;
    // 2 -- > need -x door;
    // 3 -- > need z door;
    // 4 -- > need -z door;

    private RoomTemplates templates;
    private int rand;
    private bool spawned = false;

    public float waitTime = 4f;

    void Start()
    {
        Destroy(gameObject, waitTime);
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke("Spawn", 0.1f);
    }

    void Spawn()
    {
        if (spawned == false && templates.rooms.Count < templates.maxRoom)
        {
            if (openingDirection == 1) //need x door
            {
                rand = Random.Range(0, templates.xRooms.Length);
                GameObject spawningRoom = templates.xRooms[rand];
                spawningRoom.GetComponent<AddRoom>().directionFrom = 1;
                Instantiate(templates.xRooms[rand], transform.position, spawningRoom.transform.rotation);
            }
            else if (openingDirection == 2) //need -x door
            {
                rand = Random.Range(0, templates.minusxRooms.Length);
                GameObject spawningRoom = templates.minusxRooms[rand];
                spawningRoom.GetComponent<AddRoom>().directionFrom = 2;
                Instantiate(spawningRoom, transform.position, spawningRoom.transform.rotation);
            }
            else if (openingDirection == 3) //need z door
            {
                rand = Random.Range(0, templates.zRooms.Length);
                GameObject spawningRoom = templates.zRooms[rand];
                spawningRoom.GetComponent<AddRoom>().directionFrom = 3;
                Instantiate(spawningRoom, transform.position, spawningRoom.transform.rotation);
            }
            else if (openingDirection == 4) //need -z door
            {
                rand = Random.Range(0, templates.minuszRooms.Length);
                GameObject spawningRoom = templates.minuszRooms[rand];
                spawningRoom.GetComponent<AddRoom>().directionFrom = 4;
                Instantiate(spawningRoom, transform.position, spawningRoom.transform.rotation);
            }
            spawned = true;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("SpawnPoint"))
        {
            if(other.GetComponent<RoomSpawner>().spawned == false && spawned == false)
            {
                //spawn a filled up
                Instantiate(templates.closedRoom, transform.position, Quaternion.identity);
                Destroy(gameObject);

            }
            spawned = true;
        }
    }
}
